library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE IEEE.std_logic_unsigned.ALL;

entity parallel_reg is
	port (
		rst, clk_100MHz, load: in std_logic;	
		I:	in std_logic_vector(3 downto 0);	
		O:	out std_logic_vector(3 downto 0)
	);
end parallel_reg;

architecture Behavioral of parallel_reg is
signal clk_1Hz: std_logic;
--	Uncomment for implementation
component divider	is
port (rst, clk_in: in STD_LOGIC;
		clk_out: out STD_LOGIC);
	end component; 

begin
	--	Uncomment	for	implementation	
		new_clk:	divider	port	map	(rst,	clk_100MHz,	clk_1Hz);	
	--	Comment	for	implementation	
	--clk_1Hz	<=	clk_100MHz;
	
	--We	add	the	remainder	of	the	code	of	the	register
	process(rst,clk_1Hz)
	begin
		if	rst='1'	then
			O<="0000";
		elsif clk_1Hz'event and clk_1Hz='1' then
			if	load='1'	then
				O<=I;
			end if;
		end if;
	end process;
end Behavioral;

