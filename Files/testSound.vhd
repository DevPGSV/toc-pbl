--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   17:29:36 01/26/2015
-- Design Name:   
-- Module Name:   E:/TOC/PBL/test.vhd
-- Project Name:  PBL
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: soundModule
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use ieee.numeric_std.all;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY test IS
END test;
 
ARCHITECTURE behavior OF test IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT soundModule
    PORT(
         clk : IN  std_logic;
         rst : IN  std_logic;
         sound_in : IN  std_logic_vector(2 downto 0);
         octave : IN  std_logic_vector(2 downto 0);
         wave : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal sound_in : std_logic_vector(2 downto 0) := (others => '0');
   signal octave : std_logic_vector(2 downto 0) := (others => '0');

 	--Outputs
   signal wave : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: soundModule PORT MAP (
          clk => clk,
          rst => rst,
          sound_in => sound_in,
          octave => octave,
          wave => wave
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		octave <= (others => '0');
		sound_in <= (others => '0');
		rst <= '1';
      wait for 100 ns;	
		
      wait for clk_period*10;
		rst <= '0';
		
      -- insert stimulus here 
		wait until falling_edge(clk);
		octave <= "010";
		sound_in <= "110";
		
		
		
		
--		wait for 100 ns;
--		octave <= "011";
--		sound_in <= "010";
--		wait for 1000 ns;
--		sound_in <= "111";
--		wait for 1000 ns;
--		sound_in <= "000";
--		wait for 1000 ns;
--		sound_in <= "100";
--		wait for 1000 ns;
--		sound_in <= "000";

      wait;
   end process;

END;
