---------------------------------------------------------------------------------
-- Engineer:Juan Andr�s Claramunt
-- 
-- Design Name: Tabla de semiperiodos de nota en la s�ptima octava
-- Module Name: tablanotas
-- Project Name: FPGAhero
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.ALL;

-- Tipos locales
use work.tipos.all;  --227328

entity tablanotas is 
	port (
		note		: in TNota;
		semiper	: out std_logic_vector(11 downto 0)
	);
end entity tablanotas;

architecture rom_tablanotas of tablanotas is
	-- Se�al de tipo entero escribir menos
	signal tmp : Integer range 0 to 2986;
begin

	-- Asigna el semiperiodo en funci�n de la nota
	--Do=C Re=D Mi=E Fa=F Sol=G La=A Si=B 
	with note select
		tmp	        <=	2986	when C, --101110101010 
								2660	when D, --101001100100
								2370	when E, --100101000010
								2237	when F, --100010111101
								1993	when G, --011111001001
								1776	when A, --011011110000
								1582	when B, --011000101110
									0	when others;
					
	semiper <= conv_std_logic_vector(tmp, 12);
end architecture rom_tablanotas;
