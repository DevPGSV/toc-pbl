library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FpgaHero is
	Port (
		clk : in  STD_LOGIC; -- FPGA clock
		rst : in  STD_LOGIC; -- Reset button
		clk_kb : in STD_LOGIC; --Keyboard clock
		data_kb : in STD_LOGIC; --Keyboard data		
		buzz_data : out STD_LOGIC; -- Speaker data
		hsyncb: out std_logic;	-- horizontal (line) sync
		vsyncb: out std_logic;	-- vertical (frame) sync
		rgboutput: out std_logic_vector(11 downto 0);	-- red,green,blue colors
		button: in std_logic;
		displayd : out std_logic_vector(6 downto 0);
		displayi : out std_logic_vector(6 downto 0)
	);
end FpgaHero;

architecture FpgaHero_arch of FpgaHero is

	component vgacore is
		port (
			reset: in std_logic;	-- reset
			clock100: in std_logic; -- 100 MHz
			hsyncb: out std_logic;	-- horizontal (line) sync
			vsyncb: out std_logic;	-- vertical (frame) sync
			rgboutput: out std_logic_vector(11 downto 0);	-- VGA output
			Blue1: in std_logic_vector(9 downto 0 );
			Blue2: in std_logic_vector(9 downto 0 );
			Blue3: in std_logic_vector(9 downto 0 );
			Green1: in std_logic_vector(9 downto 0 );
			Green2: in std_logic_vector(9 downto 0 );
			Green3: in std_logic_vector(9 downto 0 );
			Red1: in std_logic_vector(9 downto 0 );
			Red2: in std_logic_vector(9 downto 0 );
			Red3: in std_logic_vector(9 downto 0 )
			
			
		);
	end component;
	
	signal vsyncb_s : STD_LOGIC;
	signal Blue1: std_logic_vector(9 downto 0 );
	signal Blue2: std_logic_vector(9 downto 0 );
	signal Blue3: std_logic_vector(9 downto 0 );
	signal Green1: std_logic_vector(9 downto 0 );
	signal Green2: std_logic_vector(9 downto 0 );
	signal Green3: std_logic_vector(9 downto 0 );
	signal Red1: std_logic_vector(9 downto 0 );
	signal Red2: std_logic_vector(9 downto 0 );
	signal Red3: std_logic_vector(9 downto 0 );

	component soundModule is
		port(
			clk, rst : in std_logic;
			sound_in : in std_logic_vector(2 downto 0);
			octave	 : in std_logic_vector(2 downto 0);
			wave : out std_logic
		);
	end component;
	
	component ROM_song_0  is
	port (clk: in std_logic;
			addr: in std_logic_vector (5 downto 0);
			data_out: out std_logic_vector (6 downto 0));
	end component;
	
	
	
	component keyboard_module is
		Port (
			clk : in  STD_LOGIC;
			clk_kb : in  STD_LOGIC;
			reset : in  STD_LOGIC;
			data_kb : in  STD_LOGIC;
			data_end : out  STD_LOGIC;
			key_out : out  STD_LOGIC_VECTOR (2 downto 0)
		);
	end component;
	
	component game_controller is
		Port (
			clk : in STD_LOGIC;
			controllerOutput : out STD_LOGIC_VECTOR (2 downto 0);
			songData : in STD_LOGIC_VECTOR (6 downto 0);
			rst : in STD_LOGIC;
			
			Blue1: out std_logic_vector(9 downto 0 );
			Blue2: out std_logic_vector(9 downto 0 );
			Blue3: out std_logic_vector(9 downto 0 );
			Green1: out std_logic_vector(9 downto 0 );
			Green2: out std_logic_vector(9 downto 0 );
			Green3: out std_logic_vector(9 downto 0 );
			Red1: out std_logic_vector(9 downto 0 );
			Red2: out std_logic_vector(9 downto 0 );
			Red3: out std_logic_vector(9 downto 0 );
			vsyncb_s : in STD_LOGIC;
			buttn: in STD_LOGIC;
			Score: out std_logic_vector(7 downto 0);
			key_code : in  STD_LOGIC_VECTOR (2 downto 0);
			buzz_data: out STD_LOGIC
		);
	end component;
	
	component key2led is
	Port (
		key : in  STD_LOGIC_VECTOR (3 downto 0);
		led : out  STD_LOGIC_VECTOR (6 downto 0)
	);
	end component;

	signal soundBuss : std_logic_vector(6 downto 0);
	signal addr : std_logic_vector (5 downto 0);
	
	signal keyboard_data_end : STD_LOGIC;
	signal keyboard_key_code : STD_LOGIC_VECTOR(2 downto 0);
	
	signal controllerOutput_s : STD_LOGIC_VECTOR (2 downto 0);
	
	
	signal CScore:  std_logic_vector(7 downto 0);
	
	signal test: std_logic_vector(3 downto 0);
	signal songData_s : STD_LOGIC_VECTOR (6 downto 0);
	--signal sonido : std_logic_vector (2 downto 0);
	--alias------------------
	alias note  : std_logic_vector(2 downto 0) is soundBuss(2 downto 0);
	alias octave : std_logic_vector(2 downto 0) is soundBuss(5 downto 3);
	alias ended  : std_logic is soundBuss(6);
begin
	test <= '0'&keyboard_key_code;
	-- map all the internal components together
	keyboard   : keyboard_module port map (clk, clk_kb, rst, data_kb, keyboard_data_end, keyboard_key_code);
	controller : game_controller port map (clk, controllerOutput_s, songData_s, rst, Blue1, Blue2, Blue3, Green1, Green2, Green3, Red1, Red2, Red3, vsyncb_s,keyboard_data_end,cscore,keyboard_key_code,buzz_data);
	vga        : vgacore port map (rst, clk, hsyncb, vsyncb_s, rgboutput, Blue1, Blue2, Blue3, Green1, Green2, Green3, Red1, Red2, Red3);
	rightled   : key2led port map(cscore(3 downto 0),displayd);
	leftled   : key2led port map(cscore(7 downto 4),displayi);
	vsyncb <= vsyncb_s;
end FpgaHero_arch;



