library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity keyboard_module is
	Port (
		clk : in  STD_LOGIC;
		clk_kb : in  STD_LOGIC;
		reset : in  STD_LOGIC;
		data_kb : in  STD_LOGIC;
		data_end : out  STD_LOGIC;
		key_out : out  STD_LOGIC_VECTOR (2 downto 0)
	);
end keyboard_module;

architecture keyboard_module_arch of keyboard_module is
	signal key_code_s : STD_LOGIC_VECTOR(7 downto 0);
	signal shiftMem_s : STD_LOGIC_VECTOR(21 downto 0);
	signal key_trans_s : STD_LOGIC_VECTOR(2 downto 0);

	component shift22 is
		Port (
			clk : in  STD_LOGIC;
			reset : in  STD_LOGIC;
			data_in : in  STD_LOGIC;
			write : in  STD_LOGIC;
			data : inout  STD_LOGIC_VECTOR (21 downto 0)
		);
	end component;
	
	component keyMap is
		Port (
			key : in  STD_LOGIC_VECTOR (7 downto 0);
			output : out  STD_LOGIC_VECTOR (2 downto 0)
		);
	end component;
	begin
	
--	keyTransformer : keyMap port map(key_code_s, key_trans_s);
	shift_memory : shift22 port map(clk_kb, reset, data_kb, '1', shiftMem_s);
	
	process (clk_kb, shiftMem_s)
	begin
		if (rising_edge(clk_kb)) then
			if shiftMem_s(9 downto 2) = x"0F" then
				data_end <= '1';
				key_code_s <= shiftMem_s(19 downto 12);
			else
				--key_code_s <= (others => '0');
				data_end <= '0';
			end if;
		end if;
	end process;
	--key_out <= key_trans_s;
	key_out <= key_code_s(2 downto 0  );
end keyboard_module_arch;