----------------------------------------------------------------------------------
-- Engineer: 		 Germ�n Casta�o Rold�n
-- Original code:	 Juan Andr�s Claramunt
-- 
-- Module Name:    Sounder
-- Project Name:   FPGA-Hero
--
-- Dependencies:   tablanotas, tablanotassos
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

use work.tipos.all;

entity gensound is
	port (
		clk	: in std_logic;
		rst	: in std_logic;
		-- Nota de acuerdo a la codificaci�n escrita
		note	: in TNota;
		
		-- Aplicaci�n o no del sostenido
		sharp	: in std_logic;
		octave	: in std_logic_vector(2 downto 0);
		wave	: out std_logic
	);
end entity gensound;

architecture arch_gensound of gensound is
	-- Semiperiodo en la s�ptima octava
	signal sp_nat, sp_sos, sp : std_logic_vector(11 downto 0);	
	
	-- Semiperiodo con octava ajustada
	signal sp_final : std_logic_vector(17 downto 0);
	signal cont, cont_sig	: std_logic_vector(17 downto 0);
	signal t_wave : std_logic;
	
	signal note2 : std_logic_vector(2 downto 0);
	signal changed : std_logic;
	
	-- Componente de tabla de notas
	component tablanotas is
		port (
			note	: in std_logic_vector(2 downto 0);
			semiper	: out std_logic_vector(11 downto 0)
		);
	end component tablanotas;
	
	-- Componente de tabla de notas (con sostenido)
	component tablanotassos is
		port (
			note	: in std_logic_vector(2 downto 0);
			semiper	: out std_logic_vector(11 downto 0)
		);
	end component tablanotassos;

begin

	-- Correspondencia entre notas y semiperiodos de wave
	tablanatural : tablanotas port map (
		note => note,
		semiper => sp_nat);
	
	-- Correspondencia entre notas y semiperiodos de wave (sostenido)
	tablasostenido : tablanotassos port map (
		note => note,
		semiper => sp_sos);
	
	
	-- Multiplexor en funci�n de 'sharp'
	with sharp select
                 sp <= sp_sos	  when '1',
                       sp_nat   when others;

	-- Multiplicador en funci�n del n�mero de octava
	-- �Se podr�an usar desplazamientos en funci�n de octave?  
	with octave select
		sp_final  <=  sp & "000000"	when "000", --(2)
					"0" & sp & "00000"	when "001",
					"00" & sp & "0000"	when "010",
					"000" & sp & "000"	when "011",
					"0000" & sp & "00"	when "100",
					"00000" & sp & "0"	when "101", --(7)
					"000000" & sp			when others;
	
	-- Contador que se reinicia con cada semiperiodo
	-- y biestable de la se�al 'onda'
	counter : process (clk, rst,changed,cont_sig,sp_final) 
	begin
	
		if rst = '1' or changed = '1' then
			t_wave <= '0';
			cont <= (others => '0');
		elsif clk'event and clk = '1' then

			if cont_sig = sp_final then
				-- Reinicia el contador
				cont <= (others => '0');

				-- Invierte 'wave' cada semiperiodo
				if sp_final /= 0 then
					t_wave <= not t_wave;
				end if;
			else
				cont <= cont_sig;
			end if;

		end if;
	end process counter;

	-- El contador avanza
	cont_sig <= cont + 1;

	wave <= t_wave;

end architecture arch_gensound;
