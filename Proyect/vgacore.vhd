----------------------------------------------------------------------------------
-- Company: TOC PBL
-- Engineer: Victor Blanco Ruiz
-- 
-- Create Date:    10:57:05 11/17/2008 
-- Design Name: 
-- Module Name:    vga - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: This module deals with the VGA sync and its output, and draws the rectangles
-- and some lines on the screen.
--
-- Dependencies: VGARectangleRenderer
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;


entity vgacore is
	port
	(
		reset: in std_logic;	-- reset
		clock100: in std_logic; -- 100 MHz
		
		hsyncb: out std_logic;	-- horizontal (line) sync
		vsyncb: out std_logic;	-- vertical (frame) sync
		
		rgboutput: out std_logic_vector(11 downto 0);	-- VGA output
		
		--inputs for the different rectangles
		Blue1: in std_logic_vector(9 downto 0 );
		Blue2: in std_logic_vector(9 downto 0 );
		Blue3: in std_logic_vector(9 downto 0 );
		
		Green1: in std_logic_vector(9 downto 0 );
		Green2: in std_logic_vector(9 downto 0 );
		Green3: in std_logic_vector(9 downto 0 );
		
		Red1: in std_logic_vector(9 downto 0 );
		Red2: in std_logic_vector(9 downto 0 );
		Red3: in std_logic_vector(9 downto 0 )
		
	);
end vgacore;

		
		
architecture vgacore_arch of vgacore is

	signal hcnt: std_logic_vector(8 downto 0);	-- horizontal pixel counter
	signal vcnt: std_logic_vector(9 downto 0);	-- vertical line counter
	signal clock: std_logic;
	signal rgb:  std_logic_vector(11 downto 0);
	signal hsyncbAux : std_logic;
	SIGNAL cs : STD_LOGIC_VECTOR (1 DOWNTO 0); 
	signal hcounter : std_logic_vector(9 downto 0  );
	
	component VGARectangleRenderer is
    Port ( hcnt: in std_logic_vector(9 downto 0);
           vcnt: in std_logic_vector(9 downto 0);
			 
			  
			  top: in std_logic_vector(9 downto 0);
			  bot: in std_logic_vector(9 downto 0);
				
           left: in std_logic_vector(9 downto 0);
           right: in std_logic_vector(9 downto 0);
          
           color: in std_logic_vector(2 downto 0);
			  
			  rgb: out std_logic_vector(11 downto 0)
			 ) ;
			  
end component;

	--signals for the output of the rectanglerenderers
	signal rect1Out: std_logic_vector(11 downto 0);
	signal rect2Out: std_logic_vector(11 downto 0);
	signal rect3Out: std_logic_vector(11 downto 0);	
	signal rect4Out: std_logic_vector(11 downto 0);
	signal rect5Out: std_logic_vector(11 downto 0);
	signal rect6Out: std_logic_vector(11 downto 0);
	signal rect7Out: std_logic_vector(11 downto 0);
	signal rect8Out: std_logic_vector(11 downto 0);
	signal rect9Out: std_logic_vector(11 downto 0);
	
	--signal for the static part of the drawing
	signal rgbstatic: std_logic_vector(11 downto 0);
	
	--signal for the "bot" dimension of each rectangle
	signal Blue1Bot : std_logic_vector(9 downto 0 );
	signal Blue2Bot : std_logic_vector(9 downto 0 );
	signal Blue3Bot : std_logic_vector(9 downto 0 );

	signal Green1Bot : std_logic_vector(9 downto 0 );
	signal Green2Bot : std_logic_vector(9 downto 0 );
	signal Green3Bot : std_logic_vector(9 downto 0 );

	signal Red1Bot : std_logic_vector(9 downto 0 );
	signal Red2Bot : std_logic_vector(9 downto 0 );
	signal Red3Bot : std_logic_vector(9 downto 0 );

		
	
	
begin
	hcounter <= '0'&hcnt;
	
	--BLUE
	rect1: VGARectangleRenderer PORT MAP (hcounter,vcnt,
	Blue1,Blue1Bot,conv_std_logic_vector(30,10),conv_std_logic_vector(120,10),
	"100",rect1Out);
	
	rect4: VGARectangleRenderer PORT MAP (hcounter,vcnt,
	Blue2,Blue2Bot,conv_std_logic_vector(30,10),conv_std_logic_vector(120,10),
	"100",rect4Out);
	
	rect5: VGARectangleRenderer PORT MAP (hcounter,vcnt,
	Blue3,Blue3Bot,conv_std_logic_vector(30,10),conv_std_logic_vector(120,10),
	"100",rect5Out);

	--GREEN
	rect2: VGARectangleRenderer PORT MAP (hcounter,vcnt,
	Green1,Green1Bot,conv_std_logic_vector(150,10),conv_std_logic_vector(240,10),
	"010",rect2Out);
	rect6: VGARectangleRenderer PORT MAP (hcounter,vcnt,
	Green2,Green2Bot,conv_std_logic_vector(150,10),conv_std_logic_vector(240,10),
	"010",rect6Out);
	rect7: VGARectangleRenderer PORT MAP (hcounter,vcnt,
	Green3,Green3Bot,conv_std_logic_vector(150,10),conv_std_logic_vector(240,10),
	"010",rect7Out);


	--RED
	rect3: VGARectangleRenderer PORT MAP (hcounter,vcnt,
	Red1,Red1Bot,conv_std_logic_vector(270,10),conv_std_logic_vector(360,10),
	"001",rect3Out);
	rect8: VGARectangleRenderer PORT MAP (hcounter,vcnt,
	Red2,Red2Bot,conv_std_logic_vector(270,10),conv_std_logic_vector(360,10),
	"001",rect8Out);
	rect9: VGARectangleRenderer PORT MAP (hcounter,vcnt,
	Red3,Red3Bot,conv_std_logic_vector(270,10),conv_std_logic_vector(360,10),
	"001",rect9Out);

	--processes to calculate the "bot" dimension from the "top" height of each rectangle
	squares1: process(Blue1,Green1, Red1) begin
		Blue1Bot <= Blue1 + conv_std_logic_vector(80,10);		
		Green1Bot <=Green1 + conv_std_logic_vector(80,10);
		Red1Bot <= Red1 + conv_std_logic_vector(80,10);
	end process;
	
	squares2: process(Blue2,Green2, Red2) begin
		Blue2Bot <= Blue2 + conv_std_logic_vector(80,10);		
		Green2Bot <=Green2 + conv_std_logic_vector(80,10);
		Red2Bot <= Red2 + conv_std_logic_vector(80,10);
	end process;
	
	squares3: process(Blue3,Green3, Red3) begin
		Blue3Bot <= Blue3 + conv_std_logic_vector(80,10);		
		Green3Bot <=Green3 + conv_std_logic_vector(80,10);
		Red3Bot <= Red3 + conv_std_logic_vector(80,10);
	end process;
	
	
	hsyncb <= hsyncbAux;

	--This process is a clock divider of clock100 
	state:
	PROCESS(reset, clock100)
	BEGIN
		IF (reset = '1') THEN
			cs <= (OTHERS=>'0');
			clock <= '0';
		ELSIF (clock100'EVENT AND clock100 = '1') THEN 
			cs <= cs + 1;
			if cs="11" then 
				clock <= not clock;
			END IF;
		END IF;
	END PROCESS;

	--640x480 pixels (total). We don't reach the maximum
	A: process(clock,reset)
	begin
		-- reset asynchronously clears pixel counter
		if reset='1' then
			hcnt <= "000000000";
		-- horiz. pixel counter increments on rising edge of dot clock
		elsif (clock'event and clock='1') then
			-- horiz. pixel counter rolls-over after 381 pixels
			if hcnt<380+50 then
				hcnt <= hcnt + 1;
			else
				hcnt <= "000000000";
			end if;
		end if;
	end process;

	B: process(hsyncbAux,reset)
	begin
		-- reset asynchronously clears line counter
		if reset='1' then
			vcnt <= "0000000000";
		-- vert. line counter increments after every horiz. line
		elsif (hsyncbAux'event and hsyncbAux='1') then
			-- vert. line counter rolls-over after 528 lines
			if vcnt<527 then
				vcnt <= vcnt + 1;
			else
				vcnt <= "0000000000";
			end if;
		end if;
	end process;
	
	
	

	C: process(clock,reset)
	begin
		-- reset asynchronously sets horizontal sync to inactive
		if reset='1' then
			hsyncbAux <= '1';
		-- horizontal sync is recomputed on the rising edge of every dot clock
		elsif (clock'event and clock='1') then
			-- horiz. sync is low in this interval to signal start of a new line
			if (hcnt>=291+50 and hcnt<337+50) then -- The pulse width must be this one because of the synchronism
				hsyncbAux <= '0';
			else
				hsyncbAux <= '1';
			end if;
		end if;
	end process;

	D: process(hsyncbAux,reset)
	begin
		-- reset asynchronously sets vertical sync to inactive
		if reset='1' then
			vsyncb <= '1';
		-- vertical sync is recomputed at the end of each line of pixels
		elsif (hsyncbAux'event and hsyncbAux='1') then
			-- vert. sync is low in this interval to signal start of a new frame
			if (vcnt>=490 and vcnt<492) then
				vsyncb <= '0';
			else
				vsyncb <= '1';
			end if;
		end if;
	end process;

	
	-- Process to actually paint colors on the screen
	process(vcnt, hcnt,rect1Out,rect2Out,rect3Out,rect4Out,rect5Out,rect6Out,rect7Out,rect8Out,rect9Out )
	begin
		
		-- screen boundary
		if vcnt>20 and vcnt < 489 and hcnt> 20 and hcnt < 379 then  
		
			--merge all the outputs of the rectangles
			rgb<=(rect2Out or rect1Out OR RECT3out or rect4Out or rect5Out OR RECT6out or rect7Out or rect8Out OR RECT9out);
			
			--static draw for screen lines
			if((hcnt > 135 and hcnt < 140) or (hcnt > 250 and hcnt < 255) or (vcnt>360 and vcnt < 370 ) ) then
				rgbstatic <="111111111111";
			else
				rgbstatic <="000000000000";
			end if;
			
		else
		rgbstatic <="000000000000";
		rgb<="000000000000";
		end if;
	end process;

	-- merge rectangle outputs and static draw into a final signal for the VGA
	rgboutput<=rgb or rgbstatic;
	
end vgacore_arch;
