
----------------------------------------------------------------------------------
-- Company: TOC PBL
-- Engineer: Victor Blanco Ruiz
-- 
-- Create Date:    10:57:05 11/17/2008 
-- Design Name: 
-- Module Name:    ColumnController
-- Project Name: PBL
-- Target Devices: 
-- Tool versions: 
-- Description: This module deals with the logic of each column, its score, and its 
-- rectangle locations
-- 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;

entity columncontroller is
Port (vSync : in STD_LOGIC;
		reset: in STD_Logic;
		Rect1: out std_logic_vector(9 downto 0 );
		Rect2: out std_logic_vector(9 downto 0 );
		Rect3: out std_logic_vector(9 downto 0 );
		score: out std_logic_vector(7 downto 0 );
		button: in std_logic;
		addpeg: in std_logic
);
end columncontroller;

architecture Behavioral of columncontroller is

-- Each height controls 1 rectangle, its their location
-- the height starts at 0 on the top of the screen, and goes to 540.
-- the height can be set to 1111111111 so the rectangle is "out of the screen"
-- and can be resetted
signal Height1 :  std_logic_vector(9 downto 0 );
signal Height2 : std_logic_vector(9 downto 0);
signal Height3 : std_logic_vector(9 downto 0);
-- score
signal LocalScore:  std_logic_vector(7 downto 0 ) := "00000000"; 

-- button check. When the button is pressed, B2 will be more than B1.
-- at next Vsync, B1 is compared to B2, if b2 is more, then the button was pressed
-- so do the logic for the button, and set B1 to B2 so they are equal again
signal B1: std_logic_vector(9 downto  0);
signal B2: std_logic_vector(9 downto  0);

begin
Rect1 <= Height1;
Rect2 <= Height2;
Rect3 <= Height3;

score <= LocalScore;

--main process, using VSync as clock for the 60 hz
vsyncprc: process ( vSync,reset ) begin	
	
	if(reset ='1')then
		Height1    <= "0000000000";
		Height2    <= "1111111111";
		Height3 	  <= "1111111111";
		LocalScore <= "00000000";		
		B1 		  <= "0000000000";
	
	-- vsync event, 60 times a second
	elsif(vSync'event and vSync = '1' ) then		

	--reset height1 from limbo
	if (addpeg = '1' and Height1 = "1111111111" and Height2 > 200) then			
			Height1 <= "0000000000";
	--reset height2 from limbo	
	elsif (addpeg = '1' and Height2 = "1111111111" and Height1 > 200 and Height1 < 400 ) then			
			Height2 <= "0000000000";
				
	--button clicked	
	elsif(B1 < B2) then
		-- check if height 1 is on the good limit, add score and reset if its inside
		if( Height1 > 360 and Height1 < 501) then
				Height1 <= "1111111111";
				LocalScore <= LocalScore + 1;
				B1 <= B2;
		-- check if height 2 is on the good limit, add score and reset if its inside
		elsif( Height2 > 360 and Height2 < 501) then
			Height2 <= "1111111111";
			LocalScore <= LocalScore + 1;
			B1 <= B2;
		--uncomment to decrease score if the button is pressed at a incorrect time
		--elsif(LocalScore > "00000000") then
			--LocalScore <= LocalScore -1;
		end if;
		B1 <= B2;			
	
		
	else 		
		-- make the height1 fall
		if(Height1 < 510) then
			height1 <= Height1 + 8;
		else		
			--out of bounds, to go limbo		
			Height1 <= "1111111111";
		end if;	
		-- make the height2 fall
		if(Height2 < 510) then
			height2 <= Height2 + 8;
		else			
			--out of bounds, to go limbo
			Height2 <= "1111111111";
		end if;	
	end if;	
	
end if;
end process;

--button process, as button signal is way too fast to do in the vsync process
 btnprc :process (button,reset) 
 begin
	if(reset = '1')then
		B2 <= "0000000000";
	--we make B2 be different from B1, so the next vsync knows that the button was pressed
	elsif(button'event and button = '1') then
		B2 <= B2+1;
	end if;
 end process;
end Behavioral;

