---------------------------------------------------------------------------------
-- Company: Equipo 1
-- Engineer:Juan Andres Claramunt
-- 
-- Design Name: Tabla de semiperiodos de nota en la s�ptima octava (con
--		sostenido)
-- Module Name: tablanotassos
-- Project Name: Proyecto de TOC
-- Target Devices: Xilinx Spartan 3
-- Tool versions: Xilinx ISE 14.1
--
-- Dependencies:
--
-- Description: (de los apuntes de J.M. Mend�as)
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- Tipos locales
use work.tipos.all;

entity tablanotassos is 
	port (
		note		: in TNota;
		semiper	: out std_logic_vector(11 downto 0)
	);
end entity tablanotassos;

architecture rom_tablanotassos of tablanotassos is
	-- Se�al de tipo entero para escribir menos
	signal tmp : Integer range 0 to 2819;
begin

	-- Asigna el semiperiodo en funci�n de la nota
	--Do=C Re=D Mi=E Fa=F Sol=G La=A Si=B 
	with note select
		tmp	<=	2819	when C,
					2511	when D,
					2237	when E,
					2112	when F,
					1881	when G,
					1676	when A,
					1493	when B,
					0	when others;
					
	semiper <= conv_std_logic_vector(tmp, 12);
end architecture rom_tablanotassos;
