-------------------------------------------------------------------------------
-- Engineer:            Germ�n Casta�o Rold�n
-- 
-- Module Name:         4HzDivider
-- Project Name:        FPGA-Hero
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity fourHzDivider is
  port (
    clk, rst    : in  std_logic;
    clkOut      : out std_logic);
end fourHzDivider;

architecture Behavioral of fourHzDivider is

 signal aux : std_logic_vector (25 downto 0);
  
begin  -- Behavioral
  clockDiv : process (clk, rst)
  begin  -- process
    if rst = '1' then                   -- asynchronous reset (active low)
      clkOut <= '0';
      aux <= (others => '0');
    elsif rising_edge(clk) then  -- rising clock edge
      if aux = "0110111111111111111111111" then  --101111101011110000100000?
        clkOut <= '1';
        aux <= (others => '0');
      else
        clkOut <= '0';
        aux <= aux + 1;
      end if;
    end if;
  end process;
end Behavioral;
