----------------------------------------------------------------------------------
-- Engineer: 		 Germ�n Casta�o Rold�n
-- 
-- Module Name:    Frequency divider 
-- Project Name:   FPGA-Hero
--
-- Dependencies:   None
--
-- Description: Creation of a 25MHz clock from a 100 MHz one
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity divider is
    port (
        rst: in std_logic;
        clk_in: in std_logic; 
        clk_out: out std_logic
    );
end divider;

architecture divider_arch of divider is
 signal count: std_logic_vector(1 downto 0);
 signal clk_aux: std_logic;
  
  begin

  counter:
  process(rst, clk_in)
  begin
    if (rst='1') then
      count<= (others =>'0');
      clk_aux<='0';
    elsif(clk_in'event and clk_in='1') then
      if (count = "01") then 
      	clk_aux <= not clk_aux;
			count <= (others =>'0');
      else
			count <= std_logic_vector(unsigned(count) + to_unsigned(1, count'length));
			clk_aux<=clk_aux;
      end if;
    end if;
  end process counter;


  clk_out <= clk_aux;

end divider_arch;