----------------------------------------------------------------------------------
-- Company: TOC PBL
-- Engineer: Victor Blanco Ruiz
-- 
-- Create Date:    10:57:05 11/17/2008 
-- Design Name: 
-- Module Name:    game_controller
-- Project Name: PBL
-- Target Devices: 
-- Tool versions: 
-- Description: This module controls the game logic and the sound, 
-- it is connected to sound modules and the VGACore module for render
-- 
--
-- Dependencies: columnController, SoundModule, RomReader, musicROM
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
entity game_controller is
	Port (
		clk : in STD_LOGIC;
		controllerOutput : out STD_LOGIC_VECTOR (2 downto 0);
		songData : in STD_LOGIC_VECTOR (6 downto 0);
		rst : in STD_LOGIC;
		
		Blue1: out std_logic_vector(9 downto 0 );
		Blue2: out std_logic_vector(9 downto 0 );
		Blue3: out std_logic_vector(9 downto 0 );
		Green1: out std_logic_vector(9 downto 0 );
		Green2: out std_logic_vector(9 downto 0 );
		Green3: out std_logic_vector(9 downto 0 );
		Red1: out std_logic_vector(9 downto 0 );
		Red2: out std_logic_vector(9 downto 0 );
		Red3: out std_logic_vector(9 downto 0 );
		vsyncb_s : in STD_LOGIC;
		buttn: in STD_LOGIC;
		Score: out std_logic_vector(7 downto 0);
		key_code : in  STD_LOGIC_VECTOR (2 downto 0);
		buzz_data: out STD_LOGIC
	);
end game_controller;

architecture Behavioral of game_controller is
	
	alias resetScore : STD_LOGIC is controllerOutput(0);
	alias resetGame : STD_LOGIC is controllerOutput(1);
	alias incrementScore : STD_LOGIC is controllerOutput(2);
	
	signal memAddr_s : STD_LOGIC_VECTOR (8 downto 0);
	signal soundTone_s : STD_LOGIC_VECTOR (6 downto 0);
	
	signal clk_time_counter_s : STD_LOGIC_VECTOR (23 downto 0);
	
	signal score_s : STD_LOGIC_VECTOR(7 downto 0);
	
	signal selected, finished : std_logic;

   type STATES is (S0, S1);
   signal State, nextState : STATES;
	
	component romReader is
	port(clk, rst    : in std_logic;
             enable 	 : in std_logic;
             addr 	 : out std_logic_vector (5 downto 0));
	end component;
component soundModule is

		port(

			clk, rst : in std_logic;

			sound_in : in std_logic_vector(2 downto 0);

			octave	 : in std_logic_vector(2 downto 0);

		wave : out std_logic

		);

	end component;
------------------------------------------------------------ Music Roms
	component ROM_song_0  is

		port (clk: in std_logic;

			addr: in std_logic_vector (5 downto 0);

			data_out: out std_logic_vector (6 downto 0));

	end component;
	
	component ROM_song_pimpimpimV1 is
		port (
			clk: in std_logic;
			addr: in std_logic_vector (5 downto 0);
			data_out: out std_logic_vector (6 downto 0)
		);
	end component;
	
	component ROM_song_House is
		port (
			clk: in std_logic;
			addr: in std_logic_vector (5 downto 0);
			data_out: out std_logic_vector (6 downto 0)
		);
	end component;
	
	component ROM_song_Spider is
		port (
			clk: in std_logic;
			addr: in std_logic_vector (5 downto 0);
			data_out: out std_logic_vector (6 downto 0)
		);
	end component;
	
	component ROM_song_Chopin is
		port (
			clk: in std_logic;
			addr: in std_logic_vector (5 downto 0);
			data_out: out std_logic_vector (6 downto 0)
		);
	end component;
	
	component ROM_song_Bday is
		port (
			clk: in std_logic;
			addr: in std_logic_vector (5 downto 0);
			data_out: out std_logic_vector (6 downto 0)
		);
	end component;
	
	component ROM_song_Yankee is
		port (
			clk: in std_logic;
			addr: in std_logic_vector (5 downto 0);
			data_out: out std_logic_vector (6 downto 0)
		);
	end component;
	
	component ROM_song_Old is
		port (
			clk: in std_logic;
			addr: in std_logic_vector (5 downto 0);
			data_out: out std_logic_vector (6 downto 0)
		);
	end component;
	
	component ROM_song_Bell is
		port (
			clk: in std_logic;
			addr: in std_logic_vector (5 downto 0);
			data_out: out std_logic_vector (6 downto 0)
		);
	end component;
	
	component ROM_song_Chopin_Ode is
		port (
			clk: in std_logic;
			addr: in std_logic_vector (5 downto 0);
			data_out: out std_logic_vector (6 downto 0)
		);
	end component;
	
	component ROM_song_Saints is
		port (
			clk: in std_logic;
			addr: in std_logic_vector (5 downto 0);
			data_out: out std_logic_vector (6 downto 0)
		);
	end component;
	
	component ROM_song_test is
		port (
			clk: in std_logic;
			addr: in std_logic_vector (5 downto 0);
			data_out: out std_logic_vector (6 downto 0)
		);
	end component;
------------------------------------------------------------

	component columncontroller is
	Port (vSync : in STD_LOGIC;
		reset: in STD_Logic;
		Rect1: out std_logic_vector(9 downto 0 );
		Rect2: out std_logic_vector(9 downto 0 );
		Rect3: out std_logic_vector(9 downto 0 );
		score: out std_logic_vector(7 downto 0 );
		button: in std_logic;
		addpeg: in std_logic
	);
	end component;
	signal Rect:  std_logic_vector(9 downto 0 );
	
	
	signal score1:  std_logic_vector(7 downto 0 );
	signal score2:  std_logic_vector(7 downto 0 );
	signal score3:  std_logic_vector(7 downto 0 );
	
	signal button1: std_logic;
	signal button2:std_logic;
	signal button3:std_logic;
		signal addr : std_logic_vector (5 downto 0);

	signal soundBuss : std_logic_vector(6 downto 0);

	--alias------------------

	alias note  : std_logic_vector(2 downto 0) is soundBuss(2 downto 0);

	alias octave : std_logic_vector(2 downto 0) is soundBuss(5 downto 3);

	alias ended  : std_logic is soundBuss(6);
	
	signal peg1 : std_logic;
	signal peg2 : std_logic;
	signal peg3 : std_logic;
begin

   score_s <= score1 + score2 + score3;
	
	Score <= score_s;
	
	blueController : columncontroller port map(vsyncb_s,rst,Blue1,Blue2,Blue3, score1,button1,peg1);
   greenController : columncontroller port map(vsyncb_s,rst,Green1,Green2,Green3, score2,button2,peg2);
	redController : columncontroller port map(vsyncb_s,rst,Red1,Red2,Red3, score3,button3,peg3);

buzzer     : soundModule port map (clk, rst, note ,octave , buzz_data);

	addrGuiver : ROMReader port map(clk, rst, '1', memAddr_s(5 downto 0));-- enable <= '1'
------------------------
  	--musicRom   : ROM_song_0  port map(clk, memAddr_s(5 downto 0), soundBuss);
	musicRom   : ROM_song_pimpimpimV1  port map(clk, memAddr_s(5 downto 0), soundBuss);
	--musicRom   : ROM_song_House  port map(clk, memAddr_s(5 downto 0), soundBuss);
	--musicRom   : ROM_song_Chopin  port map(clk, memAddr_s(5 downto 0), soundBuss);
	--musicRom   : ROM_song_Bday  port map(clk, memAddr_s(5 downto 0), soundBuss);
	--musicRom   : ROM_song_Yankee  port map(clk, memAddr_s(5 downto 0), soundBuss);
	--musicRom   : ROM_song_Old  port map(clk, memAddr_s(5 downto 0), soundBuss);
	--musicRom   : ROM_song_Bell  port map(clk, memAddr_s(5 downto 0), soundBuss);
	--musicRom   : ROM_song_Chopin_Ode port map(clk, memAddr_s(5 downto 0), soundBuss);
	--musicRom   : ROM_song_Saints port map(clk, memAddr_s(5 downto 0), soundBuss);
	--musicRom   : ROM_song_test port map(clk, memAddr_s(5 downto 0), soundBuss);
	
------------------------
vsync: process(vsyncb_s,note(0)) begin
	if(vsyncb_s'event and vsyncb_s='1') then
		if(note(0) = '1') then
			peg1 <= '1';
			peg2 <= '0';
			peg3 <= '0';
		elsif(note(1) = '1') then
			peg1 <= '0';
			peg2 <= '1';
			peg3 <= '0';
		else
			peg1 <= '0';
			peg2 <= '0';
			peg3 <= '1';
			
		end if;
	end if;
end process;
keys:process(buttn, key_code) begin
	 
	
		if(buttn = '1' and key_code = "100") then
			button1 <= '1';
			button2 <= '0';
			button3 <= '0';
		elsif(buttn = '1' and key_code = "101") then
			button1 <= '0';
			button2 <= '1';
			button3 <= '0';
		elsif(buttn = '1' and key_code = "110") then
			button1 <= '0';
			button2 <= '0';
			button3 <= '1';
		else
			button1 <= '0';
			button2 <= '0';
			button3 <= '0';
		end if;	
end process;
	 
	 SYNC: process (clk, rst, selected)
    begin  -- process
		
      if rst = '1' then
        State <= S0;
      elsif rising_edge(clk) then
        State <= nextState;
      end if;
    end process SYNC;

    COMB: process (State,rst)
    begin  -- process

      case State  is
         when S0 => -- paused
				
				nextState <= S1;
				
			when S1 => --running
			
				if rst = '0' then
					nextState <= S1;
				else
					nextState <= S1;
          end if;		

      end case;
      
    end process;
	

end Behavioral;