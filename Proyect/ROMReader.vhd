-------------------------------------------------------------------------------
-- Engineer:            Germ�n Casta�o Rold�n
--
-- Module Name:         ROM Reader
-- Project Name:        FPGA-Hero
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity romReader is
	port(clk, rst    : in std_logic;
             enable 	 : in std_logic;
             addr 	 : out std_logic_vector (5 downto 0));
end romReader;

architecture Behavioral of romReader is
	
	component fourHzDivider is
		port(clk, rst    : in std_logic;
           clkOut	 : out std_logic);
	end component;
	
	signal clk2     : std_logic;
	signal counting : std_logic_vector(5 downto 0);
	
begin
 internalClock : fourHzDivider port map (clk, rst, clk2);
 
 counter : process (clk, rst)
		begin
			if rst = '1' then
				counting <= (others => '0');
		 elsif rising_edge(clk) and clk2='1' then
			if enable = '1' then
				if counting = "111111" then
					counting <= (others => '0');
				else
					counting <= counting + 1;
				end if;
			end if;
		end if;
	end process;

	addr <= counting;
	
end Behavioral;
