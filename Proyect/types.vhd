--
-- Definiciones generales
--

library ieee;
use ieee.std_logic_1164.all;

package tipos is
	-- La codificación de nota es un vector de 3 elementos
	subtype TNota is std_logic_vector(2 downto 0);

	-- Constantes de tipo TNota
	--Do=C Re=D Mi=E Fa=F Sol=G La=A Si=B 
	constant silence	: TNota	:= "000";
	constant C			: TNota	:= "001";
	constant D			: TNota	:= "010";
	constant E			: TNota	:= "011";
	constant F			: TNota	:= "100";
	constant G			: TNota	:= "101";
	constant A			: TNota	:= "110";
	constant B			: TNota	:= "111";

end package tipos;
