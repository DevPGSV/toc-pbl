library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity ROM_song_House is
	port (
		clk: in std_logic;
		addr: in std_logic_vector (5 downto 0);
		data_out: out std_logic_vector (6 downto 0)
	);
end ROM_song_House;

architecture ROM_arch of ROM_song_House is
	type rom_type is array (0 to 63) of std_logic_vector (6 downto 0);
	constant ROM_memory : rom_type:= (


    "0000000",
	"0011111",
	"0011011",
	"0010100",
	"0011101",
	"0100111",
	"0010110",
	"0011011",
	"0000000",
	"0011011",
	"0000000",
	"0010011",
	"0000000",
	"0010011",
	"0000000",
	"0100011",
	"0010010",
	"0000000",
	"0010010",
	"0010111",
	"0000000",
	"0011111",
	"0011011",
	"0010100",
	"0011101",
	"0100111",
	"0010110",
	"0011011",
	"0000000",
	"0011011",
	"0000000",
	"0011011",
	"0000000",
	"0011011",
	"0011111",
	"0000000",
	"0011111",
	"0011011",
	"1111111",
	"0000000",
	"0000000",
	"0000000",
	"0000000",
	"0000000",
	"0000000",
	"0000000",
	"0000000",
	"0000000",
	"0000000",
	"0000000",
	"0000000",
	"0000000",
	"0000000",
	"0000000",
	"0000000",
	"0000000",
	"0000000",
	"0000000",
	"0000000",
	"0000000",
	"0000000",
	"0000000",
	"0000000",
	"0000000");

begin
    puerto: process (clk)
    begin
		if rising_edge(clk) then
			data_out <= ROM_memory(conv_integer(addr));--<======
		end if;
    end process puerto;

end ROM_arch;



--https://www.youtube.com/watch?v=Ib8ETXwJDt0