library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity ROM_song_Mary is
	port (
		clk: in std_logic;
		addr: in std_logic_vector (5 downto 0);
		data_out: out std_logic_vector (6 downto 0)
	);
end ROM_song_Mary;

architecture ROM_arch of ROM_song_Mary is
	type rom_type is array (0 to 63) of std_logic_vector (6 downto 0);
	constant ROM_memory : rom_type:= (

"0011011",
"0011010",
"0011001",
"0011010",
"0011011",
"0011000",
"0011011",
"0011000",
"0011011",
"0011010",
"0011000",
"0011010",
"0011000",
"0011010",
"0011011",
"0011000",
"0011011",
"0011000",
"0011011",
"0011000",
"0011011",
"0011010",
"0011001",
"0011010",
"0011011",
"0011000",
"0011011",
"0011000",
"0011011",
"0011000",
"0011011",
"0011010",
"0011000",
"0011010",
"0011011",
"0011010",
"0011001",
"1111111",
"0000000",
"0000000",
"0000000",
"0000000",
"0000000",
"0000000",
"0000000",
"0000000",
"0000000",
"0000000",
"0000000",
"0000000",
"0000000",
"0000000",
"0000000",
"0000000",
"0000000",
"0000000",
"0000000",
"0000000",
"0000000",
"0000000",
"0000000",
"0000000",
"0000000",
"0000000");

begin
    puerto: process (clk)
    begin
		if rising_edge(clk) then
			data_out <= ROM_memory(conv_integer(addr));--<======
		end if;
    end process puerto;

end ROM_arch;