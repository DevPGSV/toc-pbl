library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity ROM_song_Yankee is
	port (
		clk: in std_logic;
		addr: in std_logic_vector (5 downto 0);
		data_out: out std_logic_vector (6 downto 0) 
	);
end ROM_song_Yankee;

architecture ROM_arch of ROM_song_Yankee is
	type rom_type is array (0 to 63) of std_logic_vector (6 downto 0);
	constant ROM_memory : rom_type:= (

"0000000",
"0000000",
"0011001",
"0000000",
"0011001",
"0011010",
"0011100",
"0011001",
"0011100",
"0011010",
"0010101",
"0011001",
"0000000",
"0011001",
"0011010",
"0011100",
"0011001",
"0011111",
"0010101",
"0011001",
"0000000",
"0011001",
"0011010",
"0011100",
"0011100",
"0011100",
"0011010",
"0011001",
"0011111",
"0010101",
"0010110",
"0011111",
"0011001",
"0000000",
"0011001",
"0010110",
"0011111",
"0010110",
"0010101",
"0010110",
"0011111",
"0011001",
"0010101",
"0010110",
"0010101",
"0010100",
"0010100",
"0010101",
"0010110",
"0011111",
"0010110",
"0010101",
"0010110",
"0011111",
"0011001",
"0010110",
"0010101",
"0011001",
"0011111",
"0011010",
"0011001",
"0000000",
"0011001",
"1111111");

begin
    puerto: process (clk)
    begin
		if rising_edge(clk) then
			data_out <= ROM_memory(conv_integer(addr));--<======
		end if;
    end process puerto;

end ROM_arch;