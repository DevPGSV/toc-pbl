library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity ROM_song_test is
	port (
		clk: in std_logic;
		addr: in std_logic_vector (5 downto 0);
		data_out: out std_logic_vector (6 downto 0)
	);
end ROM_song_test;

architecture ROM_arch of ROM_song_test is
	type rom_type is array (0 to 63) of std_logic_vector (6 downto 0);
	constant ROM_memory : rom_type:= (
	
"0001110",
"0001111",
"0001001",
"0001010",
"0001011",
"0001100",
"0001101",
"0000000",
"0000000",
"0010110",
"0010111",
"0010001",
"0010010",
"0010011",
"0010100",
"0010101",
"0000000",
"0000000",
"0011110",
"0011111",
"0011001",
"0011010",
"0011011",
"0011100",
"0011101",
"0000000",
"0000000",
"0100110",
"0100111",
"0100001",
"0100010",
"0100011",
"0100100",
"0100101",
"0000000",
"0000000",
"0101110",
"0101111",
"0101001",
"0101010",
"0101011",
"0101100",
"0101101",
"0000000",
"0000000",
"0110110",
"0110111",
"0110001",
"0110010",
"0110011",
"0110100",
"0110101",
"0000000",
"0000000",
"0111110",
"0111111",
"0111001",
"0111010",
"0111011",
"0111100",
"0111101",
"0000000",
"0000000",
"1111111");

begin
    puerto: process (clk)
    begin
		if rising_edge(clk) then
			data_out <= ROM_memory(conv_integer(addr));--<======
		end if;
    end process puerto;

end ROM_arch;