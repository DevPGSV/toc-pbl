library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity shift22 is
	Port (
		clk : in  STD_LOGIC;
		reset : in  STD_LOGIC;
		data_in : in  STD_LOGIC;
		write : in  STD_LOGIC;
		data : inout  STD_LOGIC_VECTOR (21 downto 0)
	);
end shift22;

architecture shift22_arch of shift22 is

begin
	shift : process (clk, reset, data_in, write)
	begin
		if reset = '1' then
			data <= (others => '0');
		elsif rising_edge(clk)then
			if (write = '1') then
				data (21 downto 1) <= data(20 downto 0);
				data(0) <= data_in;
			end if;
		end if;
	end process;
end shift22_arch;

