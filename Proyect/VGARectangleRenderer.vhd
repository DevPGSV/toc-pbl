----------------------------------------------------------------------------------
-- Company: TOC PBL project
-- Engineer: Victor Blanco Ruiz
-- 
-- Create Date:    18:54:12 01/26/2015 
-- Design Name: 
-- Module Name:    VGARecangleRenderer - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: This module draws a rectangle on the screen, it needs its screen
-- coordinates and the rectangle dimensions, plus a color to draw. It returns that color if the 
-- screen coordinates are inside the rectangle, black otherwise
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;



entity VGARectangleRenderer is
    Port ( hcnt: in std_logic_vector(9 downto 0); --horizontal draw coordinate
           vcnt: in std_logic_vector(9 downto 0); --vertical draw coordinate
			 
			  
			  top: in std_logic_vector(9 downto 0);
			  bot: in std_logic_vector(9 downto 0);
				
           left: in std_logic_vector(9 downto 0);
           right: in std_logic_vector(9 downto 0);
          
           color: in std_logic_vector(2 downto 0);
			  
			  
			   rgb: out std_logic_vector(11 downto 0) -- color output
			  )  ;
end VGARectangleRenderer;

architecture Behavioral of VGARectangleRenderer is

begin


output: process(hcnt,vcnt,top,bot,left,right,color) begin

	--compare the coordinates with the rectangle boundaries, return black if its outside, and "color" if its inside
	if(vcnt > top and vcnt < bot and hcnt > left and hcnt < right) then
		
			rgb <= (  color(2)&color(2)&color(2)&color(2)&
						color(1) &color(1)&color(1)&color(1)&
						color(0) &color(0)&color(0)&color(0)  )
						;
			
	else
	rgb <= "000000000000";
	end if;
	
end process;

end Behavioral;

