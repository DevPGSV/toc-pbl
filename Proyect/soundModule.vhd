----------------------------------------------------------------------------------
-- Engineer: 		 Germ�n Casta�o Rold�n
-- 
-- Module Name:    soundModule
-- Project Name:   FPGA-Hero
--
-- Dependencies:   Music ROM
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

use work.tipos.all;

entity soundModule is
	port(clk, rst	 : in std_logic;
		  sound_in	 : in std_logic_vector(2 downto 0); -- restet counters when change
		  octave		 : in std_logic_vector(2 downto 0);
		--  sharp	 : in std_logic;
		  wave		 : out std_logic
	);
end soundModule;

architecture Behavioral of soundModule is

--	component ram_notes is
--	  port(clk : in std_logic;
--			 we : in std_logic;
--			 addr : in std_logic_vector(5 downto 0);
--			 din : in std_logic_vector(3 downto 0);
--			 dout : out std_logic_vector(3 downto 0)
--			);
--	end component;
	
	component gensound is
		port(clk		: in std_logic;
			  rst		: in std_logic;
			  note	: in TNota;
			  sharp	: in std_logic;
			  octave	: in std_logic_vector(2 downto 0);
			  wave	: out std_logic
			 );
	end component gensound;
	
	component divider is
    port (
        rst: in STD_LOGIC;
        clk_in: in STD_LOGIC; 
        clk_out: out STD_LOGIC
		);
		end component divider;

	signal note   		: TNota;
	signal clk_25MHz	: std_logic;
---------------------------------------
--	signal ramout 		: std_logic_vector(3 downto 0);
--	signal we 			: std_logic;
--	signal addr 		: std_logic_vector(5 downto 0);
---------------------------------------
	
begin
	--RAM 	  : ram_notes port map(clk, we, addr, "0000", ramout);
	Sounder : gensound port map(clk_25MHz, rst, note, '0', octave, wave);
	FDIV	  : divider port map(rst, clk, clk_25MHz);
	
	--Do=C Re=D Mi=E Fa=F Sol=G La=A Si=B 
	PLAY: process(sound_in)
		begin
			case sound_in is
				when "000" =>
					note <= silence; 
				when "001" =>
					note <= C;
				when "010" =>
					note <= D;
				when "011" =>
					note <= E;
				when "100" =>
					note <= F;
				when "101" =>
					note <= G;
				when "110" =>
					note <= A;
				when "111" =>
					note <= B;
				when others =>
					note <= silence;
			end case;
		end process;
		
end Behavioral;

